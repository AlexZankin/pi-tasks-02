#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main()
{
	int number, i, j;
	char buf[256];
    printf("Enter the number of symbols: \n");
    scanf("%d",&number);
	srand(time(0));			
	for (j = 0; j < 40; ++j)
	{
		for (i = 0; i < number; ++i) 
		{
			switch(rand() % 3)
			{
				case 0:
					buf[i] = rand() % 10 + '0';
					break;
				case 1:
					buf[i] = rand() % 26 + 'A';
					break;
				case 2:
					buf[i] = rand() % 26 + 'a';
					break;
			}
		}		
		for (i = 0; i < number; ++i)
			printf("%c", buf[i]);
		if (j % 2 == 0)
			printf(" ");
		else
			printf("\n");		
	}
	return 0;
}