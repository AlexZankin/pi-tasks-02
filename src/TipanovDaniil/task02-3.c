#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
	int min = 0, max = 0; //минимум, максимум диапазона
	int i = 0;
	printf("Enter min and max (Ex.: 3-12): ");
	if ((scanf("%d-%d", &min, &max) == 0) || min > max || min < 0 || max < 0)
	{
		printf("INPUT ERROR!\n");
		return 1;
	}
	for (i = min; i <= max; i++)
	{
		if (min == max)
			printf("[%d]\n", i);
		else if (i == min)
			printf("[%d, ", i);
		else if (i == max)
			printf("%d]\n", i);
		else
			printf("%d, ", i);
	}
	return 0;
}