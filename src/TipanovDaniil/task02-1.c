#define _CRT_SECURE_NO_WARNINGS
#define N 256
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int i = 0, nums = 0, smallCh = 0, bigCh = 0; //���������� ����������, ������������ �� ������ � main (������� i � 3 �������� ��������� �������� � ������)

void passGen(char pass[N]) //�������, ������������� ������ i � ����� ������
{
	int symb = 0;
	symb = rand() % (3 - 1 + 1) + 1;
	if (symb == 1)
	{
		pass[i] = rand() % ('9' - '0' + 1) + '0';
		nums++;
	}
	else if (symb == 2)
	{
		pass[i] = rand() % ('z' - 'a' + 1) + 'a';
		smallCh++;
	}
	else if(symb == 3)
	{
		pass[i] = rand() % ('Z' - 'A' + 1) + 'A';
		bigCh++;
	}
}

int main()
{
	char pass[N];
	const k = 40;
	int length = 0, j = 0, f = 0;
	srand(time(0));
	printf("Enter password's length (minimum: 3): ");
	if (scanf("%d", &length) == 0 || length < 3)
	{
		printf("INPUT ERROR!\n");
		return 1;
	}
	for (j = 0; j < k; j++) //����� 40 �������
	{
		while (nums == 0 || smallCh == 0 || bigCh == 0) //�������� �� ��, ����� ��� ������� �������������� � ������ ������
		{
			nums = 0, smallCh = 0, bigCh = 0; //� ������, ���� ���� ���� �� ��������� ����� ����, �������� �� ��� ��������� ������ ������
			for (i = 0; i < length; i++) //������ ������ ���� ����� length, �������� ������� ���� ������������
				passGen(pass);
		}
		for (f = 0; f < i; f++)
			printf("%c", pass[f]);
		putchar(j % 2 != 0 ? '\n' : '\t');
		nums = 0, smallCh = 0, bigCh = 0;
	}
	printf("\n");
	return 0;
}