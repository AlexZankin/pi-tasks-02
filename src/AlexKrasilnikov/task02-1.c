#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>

void main()
{
	int length, tsp, simbol;
	srand(time(NULL));
	printf("Enter length of password: ");
	scanf("%d", &length);
	for (int i = 0; i < 40; i++)
	{
		for (int j = 0; j < length; j++)
		{
			tsp = rand() % 3;
			switch (tsp)
			{
			case 0:
				simbol = 48 + rand() % 10;
				break;
			case 1:
				simbol = 65 + rand() % 26;
				break;
			case 2:
				simbol = 97 + rand() % 26;
				break;
			default:
				break;
			}
			printf("%c", simbol);
		}
		if (i % 2 == 0) printf(" "); 
		else printf("\n");
	}
}