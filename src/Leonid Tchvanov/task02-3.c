#include <stdio.h>

int main()
{
    int i, firstNum, lastNum;
    printf("Enter an interval:");
    scanf("%d-%d", &firstNum, &lastNum);
    putchar('[');
    for (i=firstNum; i<lastNum; i++)
    {
        printf("%d, ", i);
    }
    printf("%d]", i);
    return 0;
}
