#define RIGHT_PASSWORD "Qwerty123"
#include <stdio.h>
#include <conio.h>
#include <string.h>

int main()
{
	char password[256], ch;
	int i=0;
	printf("Enter your password:");
	while ((ch=_getch()) != '\r')
	{
		if (ch!='\b')
        {
            putchar('*');
		    password[i] = ch;
		    i++;
        }
        else
        {
            if (i>0)
            {
                printf("\b \b"); // �������� ���������� ������� ��� ������� Backspace
                i--;
            }
        }
	}
	putchar('\n');
	password[i] = '\0';
	if (strcmp(password, RIGHT_PASSWORD)==0)
		printf("Access granted!\n");
	else
		printf("Access denied!\n");
	return 0;
}
