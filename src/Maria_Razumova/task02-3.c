/*Написать программу, раскрывающую введенный пользователем диапазон чисел в последовательность чисел,
 *  например: "3-12" -> [3,4,5,6,7,8,9,10,11,12].
 * Числа указываются только целые и положительные.*/
#include <stdio.h>
#include <string.h>
#define N 256

void openRange(int first,int last)
{
    int i=first;
    putchar('[');
    for(i=first;i<last;i++)
    {
        printf("%d, ",i);
    }
    printf("%d].",i);
}

int main(void)
{
    int first,last;
    printf("Enter range first-last: ");
    if (!scanf("%d-%d",&first,&last))
    {
        printf("Error!");
        return 1;
    }
    openRange(first,last);

    return 0;
}

