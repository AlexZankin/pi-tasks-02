#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
int main()
{
	char correctPass[] = "Qwerty12345";
	char symbol={'\0'};
	int i=0, check=0;
	puts("Enter a password. Enter '.' to confirm");
	symbol = _getch();
	while (symbol!='.')
	{
		putchar('*');
		if (symbol==correctPass[i])
			check+=1;
		else 
			check=0;
		i++;
		symbol = _getch();
	}
	printf("\n");
	if (check==strlen(correctPass))
		puts("Password is correct. Access allowed");
	else
	{
		puts("Password is incorrect. Accsess denied");
		return 1;
	}
	return 0;
}