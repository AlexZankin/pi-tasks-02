﻿#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#include <string.h>
#define N 256

int main()
{
	int begin = 0, end = 0, i = 0;
	
	printf("\n Input a interval in format a-b: \n");
	printf(" Example: 15-29 \n ");
	scanf("%d-%d",&begin,&end);

	if(begin<end){
		printf(" [ ");
		for(i=begin;i<=end;i++)
			printf("%d ",i);
		printf("]");
		// 15-20 : [ 15, 16, 17, 18, 19, 20 ]
	}else{
		printf(" [ ");
		for(i=begin;i>=end;i--)
			printf("%d ",i);
		printf("]");
		// 20-15 : [ 20, 19, 18, 17, 16, 15 ]
	}
	

	return 0;
}