﻿#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#define N 256

int mod(int i, int k){
	if(i % k == 0)
		return 0;
	else
		return 1;
}

int generatePassword(int size){

	char mask[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'v', 'x', 'y', 'z', 
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'V', 'X', 'Y', 'Z',
			'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

	int bigLet = 0, smallLet = 0, numb = 0, i = 0, current = 0, n, k;

	char pass[N] = {0};
	char firstElem, shuffle;

	int indexes[N];

	for(i=0;i<size;i++){
		indexes[i]=0;
	}

	bigLet = size / 3;
	smallLet = size /3; 
	numb = size - bigLet - smallLet; 

	n = bigLet + smallLet;
	
	for(i = 0; i<bigLet; i++)
		pass[i] = mask[rand()%22]; 
	for(i = bigLet; i<n; i++)
		pass[i]= mask[rand()%22 +22];
	for(i = n; i<size; i++)
		pass[i] = mask[rand()%10 +44];
	
	// генерация пароля 2: рандомная перемешка
	firstElem = pass[0];
	current = 0;

	while(current<size){
		
		k = rand()%size;

		if(indexes[k]==0){
			indexes[k]=1;
			current=0;
			shuffle = pass[k];
			pass[k] = firstElem;
			firstElem = shuffle;
		}else{
			current+=1;
		}
	}


	for(i = 0; i<size; i++)
		printf("%c",pass[i]); 

	return 0;
}


int main()
{

	clock_t start, finish;
	int len = 0, i = 0;
	int newpass;
	double t;
	srand(time(0));

	printf("Please, write the length for passwords generating: \n");
	scanf("%d",&len);
	
	start = clock();

	if(len < 3){
		// Для пароля длинной меньше трех не будет комбинации из двух символов разного регистра и цифры
		printf("For this length we cant generate good passwords. Sorry");
	}else{
		for(i=0; i<40; i++){
			newpass = generatePassword(len);
			printf( mod(i,2) ? "\n" : " " );
			// % 2 почему-то не работал, вынес в функцию
		}
	}

	finish = clock();

	t = (double) (finish-start) / CLOCKS_PER_SEC;

	printf("\n Time for len(%d) = %4.10f sec \n", len, t);

	return 0;
}

